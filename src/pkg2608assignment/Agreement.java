/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author chong
 */
public class Agreement {
// From the class diagram i have rid of some redundent varibles. 
    
    private int agreementID;
    private int SeekuniqueNo;
    private int OfferuniqueNo;
    private String seeker;
    private String offerer;
    private String status;
    private int payAmt;
    private LocalDate agreeDate;
    private LocalDate createDay;
    private String initateBy;
    private double offerReceivable;
    private long toPin;
    private long fromPin;

    public Agreement() {

    }

    public int getAgreementID() {
        return agreementID;
    }

    public void setAgreementID(int agreementID) {
        this.agreementID = agreementID;
    }

    public int getSeekuniqueNo() {
        return SeekuniqueNo;
    }

    public void setSeekuniqueNo(int SeekuniqueNo) {
        this.SeekuniqueNo = SeekuniqueNo;
    }

    public int getOfferuniqueNo() {
        return OfferuniqueNo;
    }

    public void setOfferuniqueNo(int OfferuniqueNo) {
        this.OfferuniqueNo = OfferuniqueNo;
    }


    public String getSeeker() {
        return seeker;
    }

    public void setSeeker(String seeker) {
        this.seeker = seeker;
    }

    public String getOfferer() {
        return offerer;
    }

    public void setOfferer(String offerer) {
        this.offerer = offerer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(int payAmt) {
        this.payAmt = payAmt;
    }

    public LocalDate getAgreeDate() {
        return agreeDate;
    }

    public void setAgreeDate(LocalDate agreeDate) {
        this.agreeDate = agreeDate;
    }


    public LocalDate getCreateDay() {
        return createDay;
    }

    public void setCreateDay(LocalDate createDay) {
        this.createDay = createDay;
    }

    public String getInitateBy() {
        return initateBy;
    }

    public void setInitateBy(String initateBy) {
        this.initateBy = initateBy;
    }


    public double getOfferReceivable() {
        return offerReceivable;
    }

    public void setOfferReceivable(double offerReceivable) {
        this.offerReceivable = offerReceivable;
    }

    public long getToPin() {
        return toPin;
    }

    public void setToPin(long toPin) {
        this.toPin = toPin;
    }

    public long getFromPin() {
        return fromPin;
    }

    public void setFromPin(long fromPin) {
        this.fromPin = fromPin;
    }





}
