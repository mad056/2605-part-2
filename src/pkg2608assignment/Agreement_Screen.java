/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;

/**
 *
 * @author chong
 */
public class Agreement_Screen extends javax.swing.JFrame {
    public static int PayAmountGlobal = 7; 
    Agreement Agreement = new Agreement();
    int AgreementID;
    Error_Dialog error = new Error_Dialog();
     boolean Controller ;

    /**
     * Creates new form NewAgreement
     */
    public Agreement_Screen() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JPanel jPanel2 = new javax.swing.JPanel();
        javax.swing.JPanel jPanel1 = new javax.swing.JPanel();
        StartingPointValue = new javax.swing.JLabel();
        BudgetAmt = new javax.swing.JLabel();
        DestinationValue = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        CreateDay = new javax.swing.JLabel();
        StatusText = new javax.swing.JLabel();
        Offerer = new javax.swing.JLabel();
        Seeker = new javax.swing.JLabel();
        Seeker1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Offerer1 = new javax.swing.JLabel();
        btnCreate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(230, 246, 255));

        jPanel1.setBackground(new java.awt.Color(204, 236, 255));

        StartingPointValue.setText("StartingPointValue");

        BudgetAmt.setText("Budget");

        DestinationValue.setText("DestinationValue");

        Date.setText("Date");

        CreateDay.setText("CreateDay");

        StatusText.setText("Status");

        Offerer.setText("Offerer");
        Offerer.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                OffererComponentHidden(evt);
            }
        });

        Seeker.setText("Offerer");
        Seeker.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                SeekerComponentHidden(evt);
            }
        });

        Seeker1.setText("Seeker");
        Seeker1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                Seeker1ComponentHidden(evt);
            }
        });

        jLabel6.setText("Status");

        jLabel1.setText("Starting Point");

        jLabel7.setText("Agreement Creation date");

        jLabel2.setText("Destination");

        jLabel4.setText("Amount");

        jLabel5.setText("Date (dd/mm)");

        Offerer1.setText("Offerer");
        Offerer1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                Offerer1ComponentHidden(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(68, 68, 68)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6)))
                    .addComponent(Offerer1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Seeker1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(Offerer)
                    .addComponent(StartingPointValue)
                    .addComponent(DestinationValue)
                    .addComponent(Date)
                    .addComponent(BudgetAmt)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(CreateDay))
                    .addComponent(StatusText)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Seeker)
                        .addGap(10, 10, 10)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(Offerer1)
                        .addGap(18, 18, 18)
                        .addComponent(Seeker1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(StartingPointValue)
                            .addComponent(jLabel1))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DestinationValue)
                            .addComponent(jLabel2))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BudgetAmt)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addComponent(Date)
                        .addGap(18, 18, 18)
                        .addComponent(StatusText)
                        .addGap(24, 24, 24)
                        .addComponent(CreateDay)
                        .addGap(18, 18, 18)
                        .addComponent(Offerer)
                        .addGap(18, 18, 18)
                        .addComponent(Seeker)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnCreate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/Create.png"))); // NOI18N
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/Cancel.png"))); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 423, Short.MAX_VALUE)
                        .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(121, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
    this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        
        
        Agreement.setAgreementID(AgreementList.AgreementList.size() + 1);
        Agreement.setStatus("Pending");
        Agreement.setPayAmt(PayAmountGlobal);
        Agreement.setAgreeDate(LocalDate.now());
        Agreement.setOfferReceivable(PayAmountGlobal);
        AgreementList.AgreementList.add(Agreement);
        JOptionPane.showMessageDialog(null, "Agreement Created.");
        this.dispose();
        
    }//GEN-LAST:event_btnCreateActionPerformed

  
    private void OffererComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_OffererComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_OffererComponentHidden

    private void Offerer1ComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_Offerer1ComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_Offerer1ComponentHidden

    private void SeekerComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_SeekerComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_SeekerComponentHidden

    private void Seeker1ComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_Seeker1ComponentHidden
        // TODO add your handling code here:
    }//GEN-LAST:event_Seeker1ComponentHidden

    
    public void LoadAgreement(int OfferUniqueNo, int SeekUniqueNo, int Starting, int Destination, String Seek, String Offer, String CreatedDate) {
        Agreement.setOfferuniqueNo(OfferUniqueNo);
        Agreement.setSeekuniqueNo(SeekUniqueNo);
        Agreement.setFromPin(Starting);
        Agreement.setToPin(Destination);
        Agreement.setSeeker(Seek);
        Agreement.setOfferer(Offer);
        Agreement.setInitateBy("Agreement");
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(CreatedDate, formatter);
        Agreement.setCreateDay(localDate);
        
        StartingPointValue.setText(Integer.toString(Starting));
        StatusText.setText("Pending");
        DestinationValue.setText(Integer.toString(Destination));
        BudgetAmt.setText("5"); 
        Date.setText(LocalDate.now().toString());
        CreateDay.setText(LocalDate.now().toString());
        Offerer.setText(Offer);
        Seeker.setText(Seek);
        CreateDay.setText(CreatedDate);
        this.repaint();
        this.setVisible(true);


      }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Agreement_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Agreement_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Agreement_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Agreement_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Agreement_Screen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    javax.swing.JLabel BudgetAmt;
    javax.swing.JLabel CreateDay;
    javax.swing.JLabel Date;
    javax.swing.JLabel DestinationValue;
    javax.swing.JLabel Offerer;
    javax.swing.JLabel Offerer1;
    javax.swing.JLabel Seeker;
    javax.swing.JLabel Seeker1;
    javax.swing.JLabel StartingPointValue;
    javax.swing.JLabel StatusText;
    javax.swing.JButton btnCancel;
    javax.swing.JButton btnCreate;
    javax.swing.JLabel jLabel1;
    javax.swing.JLabel jLabel2;
    javax.swing.JLabel jLabel4;
    javax.swing.JLabel jLabel5;
    javax.swing.JLabel jLabel6;
    javax.swing.JLabel jLabel7;
    // End of variables declaration//GEN-END:variables
}
