/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.util.HashSet;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Julian
 */
public class CarSeek {
    public static int SeekCount = 0;

    UserOption UserOption = new UserOption();

    String OwnerMember;
    String OfferMember = "";
    int OfferuniqueNo = 0;

    public String getOfferMember() {
        return OfferMember;
    }

    public void setOfferMember(String OfferMember) {
        this.OfferMember = OfferMember;
    }

    public int getOfferuniqueNo() {
        return OfferuniqueNo;
    }

    public void setOfferuniqueNo(int OfferuniqueNo) {
        this.OfferuniqueNo = OfferuniqueNo;
    }

    public String getOwnerMember() {
        return OwnerMember;
    }

    public void setOwnerMember(String OwnerMember) {
        this.OwnerMember = OwnerMember;
    }

    // Just implemenation of the whole car share match thing
    public void Checkmatch() {
        CarSeek CurrentSeek;
        CarOffer CurrentOffer;
        User CurrentUser;

        //reset. Yes it has to be done like this..
        for (int i = 0; i < OfferList.OfferList.size(); i++) {
            CurrentOffer = OfferList.OfferList.get(i);
            CurrentOffer.setSeeked(0);
        }

        for (int j = 0; j < SeekList.SeekList.size(); j++) {
            CurrentSeek = SeekList.SeekList.get(j);
            CurrentSeek.UserOption.setFlag("No Car Found");
        }

        for (int i = 0; i < OfferList.OfferList.size(); i++) {
            CurrentOffer = OfferList.OfferList.get(i);
            for (int j = 0; j < SeekList.SeekList.size(); j++) {
                CurrentSeek = SeekList.SeekList.get(j);
                // Whats better than &&? A nested if statement! Great for the eyes. 
                // Actually couldn't get this feature to work in a more astetically pleasing way. 
                if (!CurrentOffer.getOwnerMember().equals(CurrentSeek.getOwnerMember())) {
                    if (CurrentOffer.UserOption.fromPin == CurrentSeek.UserOption.fromPin == true) {
                        if (CurrentOffer.UserOption.toPin == CurrentSeek.UserOption.toPin == true) {
                            if (CurrentOffer.UserOption.date.toString().equals(CurrentSeek.UserOption.date.toString())) {
                                if (CurrentOffer.getSeeked() != CurrentOffer.getQuota()) {
                                    CurrentOffer.setSeeked(CurrentOffer.getSeeked() + 1);
                                    CurrentSeek.setOfferMember(CurrentOffer.getOwnerMember());
                                    CurrentSeek.setOfferuniqueNo(CurrentOffer.UserOption.getUniqueNo());
                                    CurrentOffer.UserOption.setFlag(CurrentOffer.getSeeked() + "/" + CurrentOffer.getQuota() + " Found");
                                    CurrentSeek.UserOption.setFlag("Match Found");

                                } else {
                                    continue;
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}
