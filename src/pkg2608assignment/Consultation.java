/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Julian
 */
public class Consultation {
    public static int ConsultCounter;
   int ConsultationNum;
   String Company_Name;
    double price;
    LocalDate date;
    
    public static ArrayList<Consultation> ConsulationList = new ArrayList<Consultation>();

    public String getCompany_Name() {
        return Company_Name;
    }

    public void setCompany_Name(String Company_Name) {
        this.Company_Name = Company_Name;
    }

    
    public int getConsultationNum() {
        return ConsultationNum;
    }

    public void setConsultationNum(int ConsultationNum) {
        this.ConsultationNum = ConsultationNum;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
 
    
    
}
