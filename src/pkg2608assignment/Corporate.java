/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.util.ArrayList;

/**
 *
 * @author ayeaye
 */
//testing
public class Corporate {

    int ID;
    String CompanyName;
    String CompanyAddress;
    int CompanyNumber;
    int CompanyCode;

    String potential ;

    public String GetPotential() {
        return potential;
    }

    public void setPotential(String potential) {
        this.potential = potential;
    }
    
    public String getCompanyAddress() {
        return CompanyAddress;
    }

    public void setCompanyAddress(String CompanyAddress) {
        this.CompanyAddress = CompanyAddress;
    }
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    public int getCompanyNumber() {
        return CompanyNumber;
    }

    public void setCompanyNumber(int CompanyNumber) {
        this.CompanyNumber = CompanyNumber;
    }

    public int getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(int CompanyCode) {
        this.CompanyCode = CompanyCode;
    }

}
