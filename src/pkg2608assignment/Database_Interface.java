/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

/**
 *
 * @author Julian
 */
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static pkg2608assignment.Login.session_index;

public class Database_Interface {

    // Simple Implementations
    // All Done through objects to reduce code and enabling reuse
    // The idea is to have all read and writes of all data we need in this class and to call it liberally.
    // Delcare
    private Connection myDBCon;
    private Statement st;
    private ResultSet rs;

    // Connection
    public void DatabaseConnect() {

        try {
            myDBCon = DriverManager.getConnection("jdbc:derby:loginDB;create=true");
            st = myDBCon.createStatement();
            //System.out.println("Connection Worked lmao");

        } catch (Exception e) {
            System.err.println("Error : " + e);
        }

    }

    public void DropTable() {
        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String DropUserTable = "DROP TABLE User_List";
            String DropOfferTable = "DROP TABLE Offer_List";
            String DropSeekTable = "DROP TABLE Seek_List";
            String DropCorpTable = "DROP TABLE Corporate_List";
            String DropPaymentTable = "DROP TABLE Payment_List";
            String DropConsultationTable = "DROP TABLE Consultation_List";
            String DropAgreementTable = "DROP TABLE Agreement_List";
            String DropMesssageTable = "DROP TABLE Message_List";
            
            st.execute(DropUserTable);
            st.execute(DropOfferTable);
            st.execute(DropSeekTable);
            st.execute(DropCorpTable);
            st.execute(DropPaymentTable);
            st.execute(DropConsultationTable);
            st.execute(DropAgreementTable);
            st.execute(DropMesssageTable);
            st.close();
        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void CreateOfferTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String OfferTable = "CREATE TABLE Offer_List" + "(" + "Quota Varchar(20)," + " Seeked VarChar(20)," + " price VarChar(20)," + " OwnerMember VarChar(20)," + " toPin VarChar(20)," + " fromPin VarChar(20)," + " date VarChar(20)," + " uniqueNo VarChar(20)," + " flag VarChar(20)," + " createDay VarChar(20)" + ")";
            st.execute(OfferTable);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    public void CreateCorporateTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String Corporate_List = "CREATE TABLE Corporate_List" + "(" + "ID Varchar(20)," + " CompanyName VarChar(20)," + " CompanyAddress VarChar(20)," + " CompanyNumber VarChar(20)," + " CompanyCode VarChar(20)," + " Potential VarChar(20)" + ")";
            st.execute(Corporate_List);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    public void CreateConsultationTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String Consultation_List = "CREATE TABLE Consultation_List" + "(" + "ID Varchar(20)," + " CompanyName VarChar(20)," + " Price VarChar(20)," + " Date VarChar(20)" + ")";
            st.execute(Consultation_List);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    public void CreateSeekTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String SeekTable = "CREATE TABLE Seek_List" + "(" + "OwnerMember Varchar(20)," + " toPin VarChar(20)," + " fromPin VarChar(20)," + " date VarChar(20)," + " uniqueNo VarChar(20)," + " flag VarChar(20)," + " createDay VarChar(20)," + " OfferuniqueNo VarChar(20)," + " OfferMember VarChar(20)" + ")";
            st.execute(SeekTable);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    public void CreatePaymentTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String PaymentTable = "CREATE TABLE Payment_List" + "(" + "amount Varchar(20)," + " date VarChar(20)," + " ID VarChar(20)," + " PaymentAccount VarChar(20)," + " PaymentType VarChar(20)," + " accountExpiry VarChar(20)," + " accountOwnerName VarChar(20)," + " paymentMedia VarChar(20)," + " PaymentTypeID VarChar(20)" + ")";
            st.execute(PaymentTable);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    public void CreateAgreementTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String PaymentTable = "CREATE TABLE Agreement_List" + "(" + "agreementID Varchar(20)," + " SeekuniqueNo VarChar(20)," + " OfferuniqueNo VarChar(20)," + " seeker VarChar(20)," + " offerer VarChar(20)," + " status VarChar(20)," + " payAmt VarChar(20)," + " agreeDate VarChar(20)," + " createDay VarChar(20)," + " initateBy VarChar(20)," + " offerReceivable VarChar(20)," + " toPin VarChar(20)," + " fromPin VarChar(20)" + ")";
            st.execute(PaymentTable);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    public void CreateTable() {
        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String User_List = "CREATE TABLE User_List" + "(" + "Username Varchar(20)," + " password VarChar(50)," + " secret VarChar(20)," + " first_name VarChar(20)," + " last_name VarChar(20)," + " userType VarChar(20)," + " gender VarChar(20)," + " age Varchar(20)," + " homeAddr VarChar(30)," + " compAddr VarChar(30)," + " companyCode Varchar(20) ," + " homeNum Varchar(20), " + " email VarChar(30)," + " vehical VarChar(30)," + "  lastmatchdate VarChar(30)," + "  commissionrate Varchar(20)," + "  CardNumber Varchar(20)," + "  CardHolderName VarChar(30)," + "  Expiary VarChar(30)," + "  CCV Varchar(20)," + "  accountExpiry VarChar(30)," + " status VarChar(30)," + " lastmatch VarChar(30)," + "  nextexpiry VarChar(30)," + "  durationtoExpiry Varchar(20)," + "  RefundFlag Varchar(20)," + "  potential Varchar(20)" + ")";
            st.execute(User_List);
            st.close();
        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }

    //message
    
        public void CreateMessageTable() {
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();
            String MessageTable = "CREATE TABLE Message_List" + "(" + "ID Varchar(20)," + " Message VarChar(7000)," + " Date VarChar(20)," + " Sender VarChar(20)," + " Receiver VarChar(20)" + ")";
            st.execute(MessageTable);
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }
    }
        
        
    // Storing and Retrieveing Data
    public void StoreUserData() {
        // some hard code
        // Check if user exists == update
        // doesn't exist == create  !

        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < UserList.UserList.size(); i++) {

                User ya = (User) UserList.UserList.get(i);
                
                
                String checkSql = "select count(*) from User_List where Username = '" + ya.getId() + "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    System.out.println(ya.getId() + " NOT FOUND");
                    String createsql = "INSERT INTO User_List VALUES('" + ya.getId() + "','" + ya.getPassword() + "','" + ya.getSecret() + "','" + ya.getFirst_name() + "','" + ya.getLast_name() + "','" + ya.getUserType() + "','" + ya.getGender() + "','" + ya.getAge() + "','" + ya.getHomeAddr() + "','" + ya.getCompAddr() + "','" + ya.getCompanyCode() + "','" + ya.getHomeNum() + "','" + ya.getEmail() + "','" + ya.getVehical() + "','" + ya.member.getLastmatchdate().toString() + "','" + ya.member.getCommissionrate() + "','" + ya.member.getCardNumber() + "','" + ya.member.getCardHolderName() + "','" + ya.member.getExpiary() + "','" + ya.member.getCCV() + "','" + ya.member.getAccountExpiry() + "','" + ya.member.membershippayment.getStatus() + "','" + ya.member.membershippayment.getLastmatch() + "','" + ya.member.membershippayment.getNextexpiry().toString() + "','" + ya.member.membershippayment.getDurationtoExpiry() + "','" + ya.member.membershippayment.getRefundFlag() + "','" + ya.member.getPotential() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    System.out.println(ya.getId() + " FOUND");
                    String updateTableSQL = "UPDATE User_List" + " SET Username = '" + ya.getId() + "'," + " password = '" + ya.getPassword() + "'," + " secret = '" + ya.getSecret() + "'," + " first_name = '" + ya.getFirst_name() + "'," + " last_name = '" + ya.getLast_name() + "'," + " userType = '" + ya.getUserType() + "'," + " gender = '" + ya.getGender() + "'," + " age = '" + ya.getAge() + "'," + " homeAddr = '" + ya.getHomeAddr() + "'," + " compAddr = '" + ya.getCompAddr() + "'," + " companyCode = '" + ya.getCompanyCode() + "'," + " homeNum = '" + ya.getHomeNum() + "'," + " email = '" + ya.getEmail() + "'," + " vehical = '" + ya.getVehical() + "'," + " lastmatchdate = '" + ya.member.getLastmatchdate().toString() + "'," + " commissionrate = '" + ya.member.getCommissionrate() + "'," + " CardNumber = '" + ya.member.getCardNumber() + "'," + " CardHolderName = '" + ya.member.getCardHolderName() + "'," + " Expiary = '" + ya.member.getExpiary() + "'," + " CCV = '" + ya.member.getCCV() + "'," + " accountExpiry = '" + ya.member.getAccountExpiry() + "'," + " status = '" + ya.member.membershippayment.getStatus() + "'," + " lastmatch = '" + ya.member.membershippayment.getLastmatch() + "'," + " nextexpiry = '" + ya.member.membershippayment.getNextexpiry() + "'," + " durationtoExpiry = '" + ya.member.membershippayment.getDurationtoExpiry() + "'," + " RefundFlag = '" + ya.member.membershippayment.getRefundFlag() + "'," + " potential = '" + ya.member.getPotential() + "'" + " WHERE Username = '" + ya.getId() + "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrieveUserData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM User_List";
            rs = st.executeQuery(query);
            while (rs.next()) {
                User test1 = new User();
                if (rs.getString("userType").equals("Staff")) test1 = new Staff();
                
                test1.member.setPotential(rs.getString("potential"));
                test1.setID(rs.getString("Username"));
                test1.setPassword(rs.getString("password"));
                test1.setSecret(rs.getString("secret"));
                test1.setFirst_name(rs.getString("first_name"));
                test1.setLast_name(rs.getString("last_name"));
                test1.setUserType(rs.getString("userType"));
                test1.setGender(rs.getString("gender"));
                test1.setAge(rs.getInt("age"));
                test1.setHomeAddr(rs.getString("homeAddr"));
                test1.setCompAddr(rs.getString("compAddr"));
                test1.setCompanyCode(rs.getLong("companyCode"));
                test1.setHomeNum(rs.getLong("homeNum"));
                test1.setEmail(rs.getString("email"));
                test1.setVehical(rs.getString("vehical"));
                test1.member.setLastmatchdate(LocalDate.parse(rs.getString("lastmatchdate"), DATE_FORMAT));
                test1.member.setCommissionrate(rs.getInt("commissionrate"));
                test1.member.setCardNumber(rs.getString("CardNumber"));
                test1.member.setCardHolderName(rs.getString("CardHolderName"));
                test1.member.setExpiary(rs.getString("Expiary"));
                test1.member.setCCV(rs.getInt("CCV"));
                test1.member.setAccountExpiry(rs.getString("accountExpiry"));
                test1.member.membershippayment.setStatus(rs.getString("status"));
                test1.member.membershippayment.setLastmatch(rs.getString("lastmatch"));
                test1.member.membershippayment.setNextexpiry(LocalDate.parse(rs.getString("nextexpiry"), DATE_FORMAT));
                test1.member.membershippayment.setDurationtoExpiry(rs.getInt("durationtoExpiry"));
                test1.member.membershippayment.setRefundFlag(rs.getInt("RefundFlag"));
                UserList.UserList.add(test1);
                System.out.println("ADDED " + rs.getString("Username"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    public void StoreOfferData() {
        // some hard code
        // Check if user exists == update
        // doesn't exist == create  !

        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < OfferList.OfferList.size(); i++) {

                CarOffer ya = (CarOffer) OfferList.OfferList.get(i);

                String checkSql = "select count(*) from Offer_List where uniqueNo = '" + ya.UserOption.getUniqueNo() + "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    System.out.println(ya.UserOption.getUniqueNo() + " NOT FOUND");
                    String createsql = "INSERT INTO Offer_List VALUES('" + ya.getQuota() + "','" + ya.getSeeked() + "','" + ya.getPrice() + "','" + ya.getOwnerMember() + "','" + ya.UserOption.getToPin() + "','" + ya.UserOption.getFromPin() + "','" + ya.UserOption.getDate().toString() + "','" + ya.UserOption.getUniqueNo() + "','" + ya.UserOption.getFlag() + "','" + ya.UserOption.getCreateDay().toString() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    System.out.println(ya.UserOption.getUniqueNo() + " FOUND");
                    String updateTableSQL = "UPDATE Offer_List" + " SET Quota = '" + ya.getQuota() + "'," + " Seeked = '" + ya.getSeeked() + "'," + " price = '" + ya.getPrice() + "'," + " OwnerMember = '" + ya.getOwnerMember() + "'," + " toPin = '" + ya.UserOption.getToPin() + "'," + " fromPin = '" + ya.UserOption.getFromPin() + "'," + " date = '" + ya.UserOption.getDate().toString() + "'," + " uniqueNo = '" + ya.UserOption.getUniqueNo() + "'," + " flag = '" + ya.UserOption.getFlag() + "'," + " createDay = '" + ya.UserOption.getCreateDay().toString()  + "' where uniqueNo = '" + ya.UserOption.getUniqueNo()+ "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrieveOfferData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Offer_List";
            rs = st.executeQuery(query);
            while (rs.next()) {
                CarOffer test1 = new CarOffer();
                test1.setQuota(rs.getInt("Quota"));
                test1.setSeeked(rs.getInt("Seeked"));
                test1.setPrice(rs.getInt("price"));
                test1.setOwnerMember(rs.getString("OwnerMember"));
                test1.UserOption.setToPin(rs.getInt("toPin"));
                test1.UserOption.setFromPin(rs.getInt("fromPin"));
                test1.UserOption.setDate(LocalDate.parse(rs.getString("date"), DATE_FORMAT));
                test1.UserOption.setUniqueNo(rs.getInt("uniqueNo"));
                test1.UserOption.setFlag(rs.getString("flag"));
                test1.UserOption.setCreateDay(LocalDate.parse(rs.getString("createDay"), DATE_FORMAT));
                OfferList.OfferList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    // Seeked
    public void StoreSeekData() {
        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < SeekList.SeekList.size(); i++) {

                CarSeek ya = (CarSeek) SeekList.SeekList.get(i);

                String checkSql = "select count(*) from Seek_List where uniqueNo = '" + ya.UserOption.getUniqueNo() + "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    System.out.println(ya.UserOption.getUniqueNo() + " NOT FOUND");
                    String createsql = "INSERT INTO Seek_List VALUES('" + ya.getOwnerMember() + "','" + ya.UserOption.getToPin() + "','" + ya.UserOption.getFromPin() + "','" + ya.UserOption.getDate().toString() + "','" + ya.UserOption.getUniqueNo() + "','" + ya.UserOption.getFlag() + "','" + ya.UserOption.getCreateDay().toString() + "','" + ya.getOfferuniqueNo() + "','" + ya.getOfferMember() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    System.out.println(ya.UserOption.getUniqueNo() + " FOUND");
                    String updateTableSQL = "UPDATE Seek_List" + " SET OwnerMember = '" + 
                            ya.getOwnerMember() + "'," + " toPin = '" + 
                            ya.UserOption.getToPin() + "'," + " fromPin = '" +
                            ya.UserOption.getFromPin() + "'," + " date = '" + 
                            ya.UserOption.getDate().toString() + "'," + 
                            " uniqueNo = '" + ya.UserOption.getUniqueNo() + 
                            "'," + " flag = '" + ya.UserOption.getFlag() + "'," + " OfferuniqueNo = '" + ya.getOfferuniqueNo() + "'," + " OfferMember = '" + ya.getOfferMember() + "' where uniqueNo = '" + ya.UserOption.getUniqueNo()+ "'" ;
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
                
            }
            st.close();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrieveSeekData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Seek_List";
            rs = st.executeQuery(query);
            while (rs.next()) {
                CarSeek test1 = new CarSeek();
                test1.setOwnerMember(rs.getString("OwnerMember"));
                test1.UserOption.setToPin(rs.getInt("toPin"));
                test1.UserOption.setFromPin(rs.getInt("fromPin"));
                test1.UserOption.setDate(LocalDate.parse(rs.getString("date"), DATE_FORMAT));
                test1.UserOption.setUniqueNo(rs.getInt("uniqueNo"));
                test1.UserOption.setFlag(rs.getString("flag"));
                test1.UserOption.setCreateDay(LocalDate.parse(rs.getString("createDay"), DATE_FORMAT));
                test1.setOfferuniqueNo(rs.getInt("OfferuniqueNo"));
                test1.setOfferMember(rs.getString("OfferMember"));
                SeekList.SeekList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    // Corporate 
    public void StoreCorporateData() {
        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < Corporate_List.CorporateList.size(); i++) {

                Corporate ya = (Corporate) Corporate_List.CorporateList.get(i);

                String checkSql = "select count(*) from Corporate_List where ID = '" + ya.getID() + "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    System.out.println(ya.getID() + " NOT FOUND");
                    String createsql = "INSERT INTO Corporate_List VALUES('" + ya.getID() + "','" + ya.getCompanyName() + "','" + ya.getCompanyAddress() + "','" + ya.getCompanyNumber() + "','" + ya.getCompanyCode() + "','" + ya.GetPotential() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    System.out.println(ya.getID() + " FOUND");
                    String updateTableSQL = "UPDATE Corporate_List" + " SET ID = '" + ya.getID() + "'," + " CompanyName  = '" + ya.getCompanyName() + "'," + " CompanyAddress = '" + ya.getCompanyAddress() + "'," + " CompanyNumber = '" + ya.getCompanyNumber() + "'," + " CompanyCode = '" + ya.getCompanyCode() + "'," + " Potential = '" + ya.GetPotential() + "' where ID = '" + ya.getID() + "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }

            }
            // st.close();
        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrieveCorporateData() {

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Corporate_List";
            rs = st.executeQuery(query);
            while (rs.next()) {
                Corporate test1 = new Corporate();
                test1.setID(rs.getInt("ID"));
                test1.setCompanyName(rs.getString("CompanyName"));
                test1.setCompanyAddress(rs.getString("CompanyAddress"));
                test1.setCompanyNumber(rs.getInt("CompanyNumber"));
                test1.setCompanyCode(rs.getInt("CompanyCode"));
                Corporate_List.CorporateList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    // Payments ! 
    public void StorePaymentData() {

        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < PaymentList.PaymentList.size(); i++) {

                Payment ya = (Payment) PaymentList.PaymentList.get(i);

                String checkSql = "select count(*) from Payment_List where ID = '" + ya.getID() + "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    //System.out.println(ya.UserOption.getUniqueNo() + " NOT FOUND");
                    String createsql = "INSERT INTO Payment_List VALUES('" + ya.getAmount() + "','" + ya.getDate().toString() + "','" + ya.getID() + "','" + ya.getPaymentAccount() + "','" + ya.getPaymentType() + "','" + ya.getAccountExpiry() + "','" + ya.getAccountOwnerName() + "','" + ya.getPaymentMedia() + "','" + ya.getPaymentTypeID() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    //System.out.println(ya.UserOption.getUniqueNo() + " FOUND");
                    String updateTableSQL = "UPDATE Payment_List" + " SET amount = '" + ya.getAmount() + "'," + " date = '" + ya.getDate().toString() + "'," + " ID = '" + ya.getID() + "'," + " PaymentAccount = '" + ya.getPaymentAccount() + "'," + " PaymentType = '" + ya.getPaymentType() + "'," + " accountExpiry = '" + ya.getAccountExpiry() + "'," + " accountOwnerName = '" + ya.getAccountOwnerName() + "'," + " paymentMedia = '" + ya.getPaymentMedia() + "'," +" PaymentTypeID = '" + ya.getPaymentTypeID() + "' where ID = '" + ya.getID() + "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrievePaymentData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Payment_List where accountOwnerName = '" + Login.session_username + "'";;
            rs = st.executeQuery(query);
            while (rs.next()) {
                Payment test1 = new Payment();
                test1.setAmount(rs.getDouble("amount"));
                test1.setDate(LocalDate.parse(rs.getString("date"), DATE_FORMAT));
                test1.setID(rs.getInt("ID"));
                test1.setPaymentAccount(rs.getString("PaymentAccount"));
                test1.setPaymentType(rs.getString("PaymentType"));
                test1.setAccountExpiry(rs.getString("accountExpiry"));
                test1.setAccountOwnerName(rs.getString("accountOwnerName"));
                test1.setPaymentMedia(rs.getString("paymentMedia"));
                test1.setPaymentTypeID(rs.getInt("PaymentTypeID"));
                PaymentList.PaymentList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    // Consulatations
    // Payments ! 
    public void StoreConsultationData() {

        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < Consultation.ConsulationList.size(); i++) {

                Consultation ya = (Consultation) Consultation.ConsulationList.get(i);

                String checkSql = "select count(*) from Consultation_List where ID = '" + ya.getConsultationNum() + "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    //System.out.println(ya.UserOption.getUniqueNo() + " NOT FOUND");
                    String createsql = "INSERT INTO Consultation_List VALUES('" + ya.getConsultationNum() + "','" + ya.getCompany_Name() + "','" + ya.getPrice() + "','" + ya.getDate().toString() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    //System.out.println(ya.UserOption.getUniqueNo() + " FOUND");
                    String updateTableSQL = "UPDATE Consultation_List" + " SET ID = '" + ya.getConsultationNum() + "'," + " CompanyName = '" + ya.getCompany_Name() + "'," + " Price = '" + ya.getPrice() + "',"  +" Date = '" + ya.getDate().toString() + "' where ID = '" + ya.getConsultationNum()+ "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrieveConsultationData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Consultation_List";
            rs = st.executeQuery(query);
            while (rs.next()) {
                Consultation test1 = new Consultation();
                test1.setConsultationNum(rs.getInt("ID"));
                test1.setCompany_Name(rs.getString("CompanyName"));
                test1.setPrice(rs.getDouble("Price"));
                test1.setDate(LocalDate.parse(rs.getString("Date"), DATE_FORMAT));
                Consultation.ConsulationList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    // Agreements
    public void StoreAgreementData() {
        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < AgreementList.AgreementList.size(); i++) {

                Agreement ya = (Agreement) AgreementList.AgreementList.get(i);

                String checkSql = "select count(*) from Agreement_List where agreementID = '" + ya.getAgreementID()+ "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    System.out.println(ya.getAgreementID() + " NOT FOUND");
                    String createsql = "INSERT INTO Agreement_List VALUES('" + ya.getAgreementID() + "','" + ya.getSeekuniqueNo() + "','" + ya.getOfferuniqueNo() + "','" + ya.getSeeker() + "','" + ya.getOfferer() + "','" + ya.getStatus() + "','" + ya.getPayAmt() + "','" + ya.getAgreeDate().toString() + "','" + ya.getCreateDay().toString() + "','" + ya.getInitateBy() + "','" + ya.getOfferReceivable() + "','" + ya.getToPin() + "','" + ya.getFromPin() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    System.out.println(ya.getAgreementID() + " FOUND");
                    String updateTableSQL = "UPDATE Agreement_List" + " SET agreementID = '" + ya.getAgreementID() + "'," + " SeekuniqueNo = '" + ya.getSeekuniqueNo() + "'," + " OfferuniqueNo = '" + ya.getOfferuniqueNo() + "'," + " seeker = '" + ya.getSeeker() + "'," + " offerer = '" + ya.getOfferer() + "'," + " status = '" + ya.getStatus() + "'," + " payAmt = '" + ya.getPayAmt() + "'," + " agreeDate = '" + ya.getAgreeDate().toString() + "'," + " createDay = '" + ya.getCreateDay().toString() + "'," + " initateBy  = '" + ya.getInitateBy() + "'," + " offerReceivable  = '" + ya.getOfferReceivable() + "'," + " toPin  = '" + ya.getToPin() + "'," + " fromPin  = '" + ya.getFromPin() + "' where agreementID = '" + ya.getAgreementID()+ "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
               // st.close();

            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    public void RetrieveAgreementData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Agreement_list";
            rs = st.executeQuery(query);
            while (rs.next()) {
                Agreement test1 = new Agreement();
                
                if (!rs.getString("initateBy").equals("Agreement")) test1 = new Adjustment();
                
                test1.setAgreementID(rs.getInt("agreementID"));
                test1.setSeekuniqueNo(rs.getInt("SeekuniqueNo"));
                test1.setOfferuniqueNo(rs.getInt("OfferuniqueNo"));
                test1.setInitateBy(rs.getString("initateBy"));
                test1.setOfferReceivable(rs.getDouble("offerReceivable"));
                test1.setToPin(rs.getInt("toPin"));
                test1.setFromPin(rs.getInt("fromPin"));
                test1.setCreateDay(LocalDate.parse(rs.getString("createDay"), DATE_FORMAT));
                test1.setPayAmt(rs.getInt("payAmt"));
                test1.setSeeker(rs.getString("seeker"));
                test1.setAgreeDate(LocalDate.parse(rs.getString("agreeDate"), DATE_FORMAT));
                test1.setStatus(rs.getString("status"));
                test1.setOfferer(rs.getString("offerer"));
                AgreementList.AgreementList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    
    // Messages
    
        public void RetrieveMessageData() {
        final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * FROM Message_List";
            rs = st.executeQuery(query);
            while (rs.next()) {
                Message test1 = new Message();
                test1.setID(rs.getInt("ID"));
                test1.setMessage(rs.getString("Message"));
                test1.setSender(rs.getString("Sender"));
                test1.setReceiver(rs.getString("Receiver"));
                test1.setSendDate(LocalDate.parse(rs.getString("Date"), DATE_FORMAT));
                MessageList.MessageList.add(test1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }

    }

    // Messages
    public void StoreMessageData() {
        // Need to not overwrite previous Offers? But How? 
        //Unique Number provides this . 
        try {

            this.DatabaseConnect();
            st = myDBCon.createStatement();

            for (int i = 0; i < MessageList.MessageList.size(); i++) {

                Message ya = (Message) MessageList.MessageList.get(i);

                String checkSql = "select count(*) from Message_List where ID = '" + ya.getID()+ "'";

                ResultSet result = st.executeQuery(checkSql);
                result.next();
                if (result.getInt(1) == 0) {
                    System.out.println(ya.getID()+ " NOT FOUND");
                    String createsql = "INSERT INTO Message_List VALUES('" + ya.getID()+ "','" + ya.getMessage()+ "','" + ya.getSendDate().toString() + "','" + ya.getSender()+ "','" + ya.getReceiver() + "')";
                    System.out.println(createsql);
                    st.execute(createsql);

                } else {
                    System.out.println(ya.getID()+ " FOUND");
                    String updateTableSQL = "UPDATE Message_List" + " SET ID = '" + ya.getID() + "'," + " Message = '" + ya.getMessage() + "'," + " Date = '" +ya.getSendDate().toString() + "'," + " Sender = '" +ya.getSender() + "'," + " Receiver = '" + ya.getReceiver() + "' where ID = '" + ya.getID()+ "'";
                    System.out.println(updateTableSQL);
                    st.execute(updateTableSQL);
                }
               // st.close();

            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }

    // Remove From Table
    public void RemoveFromTable(String Table, String Name, String Value, String PrimaryName, String Primary) {
        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement();
            System.out.println("DELETE FROM " + Table + " WHERE " + Name + " = '" + Value + "' and " + PrimaryName + " = '" + Primary + "'");
            String Remove = "DELETE FROM " + Table + " WHERE " + Name + " = '" + Value + "' and " + PrimaryName + " = '" + Primary + "'";
            st.execute(Remove);
            st.close();
        } catch (SQLException e) {

            System.out.println(e.getMessage());
        }

    }
    
      public boolean Exists() {
// You're probably reading this
// using exceptions to work out database existance == not that good but works

        try {
            this.DatabaseConnect();
            st = myDBCon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String DropUserTable = "SELECT * FROM User_List";
            st.execute(DropUserTable);
            return true;
            

            

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;

        }
      }
    
}
