package pkg2608assignment;

import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.swing.UIManager;

/**
 *
 * @author Julian
 */
public class Main extends Application {
    
    @Override
    public void start(Stage stage) throws IOException {

        try {
            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        /*Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
 
        File file = new File("src/pkg2608assignment/Resources/Ubuntu-Regular.ttf");
        Font font = null;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, file).deriveFont(Font.BOLD, 12);
        } catch (Exception e) {
            System.out.println(e);
            font = new Font("Arial", Font.BOLD, 12);
        }
 
        setUIFont(font);
        */
        
        new Login().setVisible(true);
            
    }
    
    private static void setUIFont (Font f){
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value != null && value instanceof javax.swing.plaf.FontUIResource) {
                System.out.println(key);
                UIManager.put(key, f);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        launch(args);
    }
    
}
