/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.util.Date;
import java.util.Vector;

/**
 *
 * @author Julian
 */
public class Member {


private String potential; 
    private LocalDate lastmatchdate= LocalDate.now();
    private int commissionrate = 0;
    private String CardNumber = "";
    private String CardHolderName = "";
    private String Expiary = "";
    private int CCV = 0;
    private String accountExpiry = "";
    private String PaymentMedia = "";
    // Corporate member?
    MemberShipPayment membershippayment = new MemberShipPayment();

    public String getPotential() {
        return potential;
    }

    public void setPotential(String potential) {
        this.potential = potential;
    }
    
    
    
    
    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String CardNumber) {
        this.CardNumber = CardNumber;
    }

    public String getCardHolderName() {
        return CardHolderName;
    }

    public void setCardHolderName(String CardHolderName) {
        this.CardHolderName = CardHolderName;
    }

    public String getExpiary() {
        return Expiary;
    }

    public void setExpiary(String Expiary) {
        this.Expiary = Expiary;
    }

    public int getCCV() {
        return CCV;
    }

    public void setCCV(int CCV) {
        this.CCV = CCV;
    }
    
    

    
    public LocalDate getLastmatchdate() {
        return lastmatchdate;
    }

    public void setLastmatchdate(LocalDate lastmatchdate) {
        this.lastmatchdate = lastmatchdate;
    }

    public int getCommissionrate() {
        return commissionrate;
    }

    public void setCommissionrate(int commissionrate) {
        this.commissionrate = commissionrate;
    }



    public String getAccountExpiry() {
        return accountExpiry;
    }

    public void setAccountExpiry(String accountExpiry) {
        this.accountExpiry = accountExpiry;
    }

    public String getPaymentMedia() {
        return PaymentMedia;
    }

    public void setPaymentMedia(String PaymentMedia) {
        this.PaymentMedia = PaymentMedia;
    }


    public MemberShipPayment getMembershippayment() {
        return membershippayment;
    }

    public void setMembershippayment(MemberShipPayment membershippayment) {
        this.membershippayment = membershippayment;
    }

    
    // 
}
