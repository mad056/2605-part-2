/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.Date;

/**
 *
 * @author Julian
 */
public class MemberShipPayment {
    //I assume that the members pays per month. If the member does not use the service he/she doesn’t get a refund. 
    //Only refund is provided if they don’t get a match which should be implemented in the Car Seek class? 
    public static int membershipfee = 10;
    private String status = "";
    private String lastmatch = "";
    private LocalDate nextexpiry = LocalDate.now();
    private int durationtoExpiry = 0;
    private int RefundFlag = 0;

    // Normal Getters and Setters 
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastmatch() {
        return lastmatch;
    }

    public void setLastmatch(String lastmatch) {
        this.lastmatch = lastmatch;
    }

    public LocalDate getNextexpiry() {
        return nextexpiry;
    }

    public void setNextexpiry(LocalDate nextexpiry) {
        this.nextexpiry = nextexpiry;
    }

    public int getDurationtoExpiry() {
        return durationtoExpiry;
    }

    public void setDurationtoExpiry(int durationtoExpiry) {
        this.durationtoExpiry = durationtoExpiry;
    }

    public int getRefundFlag() {
        return RefundFlag;
    }

    public void setRefundFlag(int RefundFlag) {
        this.RefundFlag = RefundFlag;
    }

    // Need a way to initialize payment 
    private void payment() {
        //set the payment type as membership payment? then intiialize the payment class?
    }

    //Way to get the next expiry (Done during the payment)
    public void UpdateExpiry() {
        // Update expiry +1 month from payment) 
    }

    //Way to refund 
    // ( set payment type as refund) and pass the parameters from the payment class)
    public void UpdateStatus() {
        //updateStatue
        //check if date > nextExpiry
    }

    public void UpdateDurationExpiry() {
        this.setDurationtoExpiry((int) DAYS.between(LocalDate.now(), nextexpiry)); 
    }

}
