/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;

/**
 *
 * @author Julian
 */
public class Message {
    int ID;
    String Message; 
    LocalDate SendDate;
    String Sender;
    String Receiver; 

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public LocalDate getSendDate() {
        return SendDate;
    }

    public void setSendDate(LocalDate SendDate) {
        this.SendDate = SendDate;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String Sender) {
        this.Sender = Sender;
    }

    public String getReceiver() {
        return Receiver;
    }

    public void setReceiver(String Receiver) {
        this.Receiver = Receiver;
    }
    
    
}
