/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Julian
 */
public class Payment {
    
    protected double amount;
    protected LocalDate date;
    private int ID;
    private String PaymentAccount;
    private String PaymentType;
    private int PaymentTypeID;
    private String accountExpiry;
    private String accountOwnerName;
    private String paymentMedia;

    public int getPaymentTypeID() {
        return PaymentTypeID;
    }

    public void setPaymentTypeID(int PaymentTypeID) {
        this.PaymentTypeID = PaymentTypeID;
    }
    
    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPaymentAccount() {
        return PaymentAccount;
    }

    public void setPaymentAccount(String PaymentAccount) {
        this.PaymentAccount = PaymentAccount;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String PaymentType) {
        this.PaymentType = PaymentType;
    }

    public String getAccountExpiry() {
        return accountExpiry;
    }

    public void setAccountExpiry(String accountExpiry) {
        this.accountExpiry = accountExpiry;
    }

    public String getAccountOwnerName() {
        return accountOwnerName;
    }

    public void setAccountOwnerName(String accountOwnerName) {
        this.accountOwnerName = accountOwnerName;
    }

    public String getPaymentMedia() {
        return paymentMedia;
    }

    public void setPaymentMedia(String paymentMedia) {
        this.paymentMedia = paymentMedia;
    }


    // Method to make payment and store it into the database
    
    //method to refunbd payment and database
    
    // method to check payments? 
    
    

}
