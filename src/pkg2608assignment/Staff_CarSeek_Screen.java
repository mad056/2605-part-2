/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Julian
 */
public class Staff_CarSeek_Screen extends javax.swing.JFrame {

    CarSeek ya;
    Database_Interface Database_Interface = new Database_Interface();
    Error_Dialog error = new Error_Dialog();

    /**
     * Creates new form Staff_CarSeek_Screen
     */
    public Staff_CarSeek_Screen() {
        initComponents();

        try {
            for (int i = 0; i < SeekList.SeekList.size(); i++) {
                ya = (CarSeek) SeekList.SeekList.get(i);
                DefaultTableModel model = (DefaultTableModel) SeekTable.getModel();
                Vector row = new Vector();
                model.addRow(row);
                //  System.out.println(SeekList.SeekList.size());
                for (int j = 0; j < SeekTable.getColumnCount(); j++) {
                    try {
                        //   System.out.println(i + " " + j);
                        switch (j) {
                            case 0:
                                SeekTable.setValueAt(ya.UserOption.getUniqueNo(), i, j);
                                break;
                            case 1:
                                SeekTable.setValueAt(ya.UserOption.getCreateDay(), i, j);
                                break;
                            case 2:
                                SeekTable.setValueAt(ya.UserOption.getToPin(), i, j);
                                break;
                            case 3:
                                SeekTable.setValueAt(ya.UserOption.getFromPin(), i, j);
                                break;
                            case 4:
                                SeekTable.setValueAt(ya.UserOption.getDate().toString(), i, j);
                                break;

                            case 5:
                                SeekTable.setValueAt(ya.UserOption.getFlag(), i, j);
                                break;

                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println(e);

                    }
                }
            }
        } catch (Exception e) {
            //System.err.println("Error : " + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        SeekTable = new javax.swing.JTable();
        Remove = new javax.swing.JButton();
        Back = new javax.swing.JButton();
        Save = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        Help3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(230, 246, 255));

        SeekTable.setBackground(new java.awt.Color(204, 236, 255));
        SeekTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Creation", "To", "From", "Date", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SeekTable.setGridColor(new java.awt.Color(204, 236, 255));
        SeekTable.setRequestFocusEnabled(false);
        SeekTable.setSelectionForeground(new java.awt.Color(230, 246, 255));
        jScrollPane1.setViewportView(SeekTable);

        Remove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/Remove.png"))); // NOI18N
        Remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoveActionPerformed(evt);
            }
        });

        Back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/BackB.png"))); // NOI18N
        Back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackActionPerformed(evt);
            }
        });

        Save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/Save.png"))); // NOI18N
        Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(230, 246, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Staff Car Seek");

        Help3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/helpedged.png"))); // NOI18N
        Help3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Help3MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Remove, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Save, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(28, 28, 28)
                        .addComponent(Help3))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel1))
                    .addComponent(Help3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 485, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Save, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Remove, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveActionPerformed
        // Add Information to new object. Add Object to list
        try {
            if (this.ControllerCheck() == true) {

                for (int i = 0; i < SeekTable.getRowCount(); i++) {
                    if (i < SeekList.SeekList.size()) {
                        ya = (CarSeek) SeekList.SeekList.get(i);
                        int temp = (Integer) SeekTable.getValueAt(i, 0);
                        if (ya.UserOption.getUniqueNo() == temp) {
                            for (int j = 0; j < SeekTable.getColumnCount(); j++) {
                                switch (j) {
                                    case 2:
                                        ya.UserOption.setToPin((Integer) SeekTable.getValueAt(i, j));
                                        break;
                                    case 3:
                                        ya.UserOption.setFromPin((Integer) SeekTable.getValueAt(i, j));
                                        break;
                                    case 4:
                                        String date = (String) SeekTable.getValueAt(i, j);
                                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                                        LocalDate localDate = LocalDate.parse(date, formatter);
                                        ya.UserOption.setDate(localDate);
                                        break;

                                }

                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            //System.err.println("Error : " + e);
        }
    }//GEN-LAST:event_SaveActionPerformed

    private void RemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoveActionPerformed
        try {
// Delete Row
            int row = SeekTable.getSelectedRow();

            int id = (Integer) SeekTable.getValueAt(row, 0);
            for (int i = 0; i < SeekList.SeekList.size(); i++) {
                ya = (CarSeek) SeekList.SeekList.get(i);
                if (ya.UserOption.getUniqueNo() == id) {
                    SeekList.SeekList.remove(i);
                    Database_Interface.RemoveFromTable("Seek_List", "uniqueNo", Integer.toString(ya.UserOption.getUniqueNo()), "OwnerMember", ya.getOwnerMember());
                    System.out.println("REMNOVES");
                }
            }
            DefaultTableModel dm = (DefaultTableModel) SeekTable.getModel();
            dm.removeRow(row);
            CarSeek.SeekCount -= 1;

            // Delete Out of SQL
        } catch (Exception e) {
            //System.err.println("Error : " + e);
        }
    }//GEN-LAST:event_RemoveActionPerformed

    private void BackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackActionPerformed
       this.dispose();
    }//GEN-LAST:event_BackActionPerformed

    private void Help3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Help3MouseClicked
Help_Dialog help = new Help_Dialog();
        help.ShowHelp("View the list of car seeks in a staff’s view.\n" +
"Click Remove to delete a record.\n" +
"Click Save to save the changes.");
                                        }//GEN-LAST:event_Help3MouseClicked

    public boolean ControllerCheck() {
        try {
            for (int i = 0; i < SeekTable.getRowCount(); i++) {
                if (error.IsInt(Integer.toString((int) SeekTable.getValueAt(i, 2)), Integer.toString((int) SeekTable.getValueAt(i, 2))) == false) {
                    return false;
                }

                if (error.IsDateFormat((String) SeekTable.getValueAt(i, 1), (String) SeekTable.getValueAt(i, 4)) == false) {
                    return false;
                }

            }
            return true;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "A serious error has occured: Please restart your application or change your inputs.");
            return false;
            //System.err.println("Error : " + e);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Staff_CarSeek_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Staff_CarSeek_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Staff_CarSeek_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Staff_CarSeek_Screen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Staff_CarSeek_Screen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Back;
    private javax.swing.JLabel Help3;
    private javax.swing.JButton Remove;
    private javax.swing.JButton Save;
    private javax.swing.JTable SeekTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
