/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 *
 * @author Julian
 */
public class Staff_Mailing_List extends javax.swing.JFrame {

    /**
     * Creates new form Staff_Mailing_List
     */
    public Staff_Mailing_List() {
        initComponents();
        
        
        /*for (int i = 0; i < UserList.UserList.size(); i++) {
            User ya = (User) UserList.UserList.get(i);
            if ((ya.getUserType().equals("Member")) && ya.member.getPotential().equals("False")) {
                DataArea.append(ya.getEmail() + "\n");
            }
        } */
        GroupBox.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
               // GroupBox.removeAllItems();
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    DataArea.setText(null);
                    switch ((String) GroupBox.getSelectedItem()) {
                        case "Potential Members":
                            for (int i = 0; i < UserList.UserList.size(); i++) {
                                User ya = (User) UserList.UserList.get(i);
                                if ((ya.getUserType().equals("Member")) && ya.member.getPotential().equals("True")) {
                                    DataArea.append(ya.getEmail() + "\n");
                                }
                            }
                            break;

                        case "Potential Corporate Members":
                            for (int i = 0; i < Corporate_List.CorporateList.size(); i++) {
                                Corporate ya = (Corporate) Corporate_List.CorporateList.get(i);
                                if (ya.GetPotential().equals("True")) {
                                    DataArea.append(ya.getCompanyAddress() + "\n");
                                }
                            }
                            break;

                        case "Members":
                            for (int i = 0; i < UserList.UserList.size(); i++) {
                                User ya = (User) UserList.UserList.get(i);
                                if ((ya.getUserType().equals("Member")) && ya.member.getPotential().equals("False")) {
                                    DataArea.append(ya.getEmail() + "\n");
                                }
                            }
                            break;
                        case "Staff":
                            for (int i = 0; i < UserList.UserList.size(); i++) {
                                User ya = (User) UserList.UserList.get(i);
                                if ((ya.getUserType().equals("Staff")) && ya.member.getPotential().equals("False")) {
                                    DataArea.append(ya.getEmail() + "\n");
                                }
                            }
                            break;
                        case "Corporate Members":
                            for (int i = 0; i < Corporate_List.CorporateList.size(); i++) {
                                Corporate ya = (Corporate) Corporate_List.CorporateList.get(i);
                                if (ya.GetPotential().equals("False")) {
                                    DataArea.append(ya.getCompanyAddress() + "\n");
                                }
                            }
                            break;
                    }

                }
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        DataArea = new javax.swing.JTextArea();
        GroupBox = new javax.swing.JComboBox<>();
        Back = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        Help2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(230, 246, 255));

        DataArea.setEditable(false);
        DataArea.setColumns(20);
        DataArea.setRows(5);
        jScrollPane1.setViewportView(DataArea);

        GroupBox.setBackground(new java.awt.Color(2, 141, 238));
        GroupBox.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 15)); // NOI18N
        GroupBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Potential Members", "Potential Corporate Members", "Members", "Staff", "Corporate Members" }));
        GroupBox.setSelectedIndex(2);
        GroupBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GroupBoxActionPerformed(evt);
            }
        });

        Back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/BackB.png"))); // NOI18N
        Back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(230, 246, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Staff Mailing List");

        Help2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pkg2608assignment/Resources/helpedged.png"))); // NOI18N
        Help2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Help2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 589, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(GroupBox, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(Help2))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(Help2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(GroupBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Back, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackActionPerformed
                    DataArea.setText(null);
        this.dispose();
    }//GEN-LAST:event_BackActionPerformed

    private void GroupBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GroupBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_GroupBoxActionPerformed

    private void Help2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Help2MouseClicked
        Help_Dialog help = new Help_Dialog();
        help.ShowHelp("View emails or addresses of users or corporate members in different groups.");
    }//GEN-LAST:event_Help2MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Staff_Mailing_List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Staff_Mailing_List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Staff_Mailing_List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Staff_Mailing_List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Staff_Mailing_List().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Back;
    private javax.swing.JTextArea DataArea;
    private javax.swing.JComboBox<String> GroupBox;
    private javax.swing.JLabel Help2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
