/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

/**
 *
 * @author Josh
 */
public class User {

    private String secret = "";
    private String id = "";

    private String first_name = "";
    private String last_name = "";
    private String userType = "";
    private String gender = "";
    private String password = "";
    //private boolean gender;
    private int age = 0;
    private String homeAddr = "";
    
    // This is the Corporate Member if they have one :-) 
    private String compAddr = "";
    private long companyCode = 0;
    private long homeNum = 0;
    private String email = "";
    private String vehical = "";

    //constructor
    // Having Member inside of User? Genius/10
    Member member = new Member();

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
 

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public User() {

    }

    public User(String ID) {
        this.id = ID;
    }

    public String getId() {
        return id;
    }

    public String getUserType() {
        return userType;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getHomeAddr() {
        return homeAddr;
    }

    public String getCompAddr() {
        return compAddr;
    }

    public long getCompanyCode() {
        return companyCode;
    }

    public long getHomeNum() {
        return homeNum;
    }

    public String getEmail() {
        return email;
    }

    public String getVehical() {
        return vehical;
    }

    //edit profile
    public void setID(String id) {
        this.id = id;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHomeAddr(String homeAddr) {
        this.homeAddr = homeAddr;
    }

    public void setCompAddr(String compAddr) {
        this.compAddr = compAddr;
    }

    public void setCompanyCode(long companyCode) {
        this.companyCode = companyCode;
    }

    public void setHomeNum(long homeNum) {
        this.homeNum = homeNum;
    }

 
    public void setEmail(String email) {
        this.email = email;
    }

    public void setVehical(String vehical) {
        this.vehical = vehical;
    }

}
