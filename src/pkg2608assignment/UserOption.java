/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2608assignment;

import java.time.LocalDate;
import java.util.Vector;

/**
 *
 * @author Josh
 */
public class UserOption {

    protected int toPin;
    protected int fromPin;
    protected long pUpFrom;
    protected long pUpto;
    protected LocalDate date;
    protected int uniqueNo;
    protected String flag;
    protected LocalDate createDay;

    public int getToPin() {
        return toPin;
    }

    public void setToPin(int toPin) {
        this.toPin = toPin;
    }

    public int getFromPin() {
        return fromPin;
    }

    public void setFromPin(int fromPin) {
        this.fromPin = fromPin;
    }

    /*
    public long getpUpFrom() {
        return pUpFrom;
    }

    public void setpUpFrom(long pUpFrom) {
        this.pUpFrom = pUpFrom;
    }

    public long getpUpto() {
        return pUpto;
    }

    public void setpUpto(long pUpto) {
        this.pUpto = pUpto;
    }
*/
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getUniqueNo() {
        return uniqueNo;
    }

    public void setUniqueNo(int uniqueNo) {
        this.uniqueNo = uniqueNo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public LocalDate getCreateDay() {
        return createDay;
    }

    public void setCreateDay(LocalDate createDay) {
        this.createDay = createDay;
    }

}
